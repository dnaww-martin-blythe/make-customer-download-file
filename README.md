# README #

This is a python3 script to generate the Living DNA customer download *.txt file.

parse the script a barcode id, eg. LD0100042A

### How do I get set up? ###

requires boto3 and argparse

you need to set the following environment varianbles for aws:

export AWS_KEY_ACCESS=
export AWS_KEY_SECRET=

export AWS_S3_BUCKET_GET=
export AWS_S3_BUCKET_PUT=

export AWS_S3_GET_REGION=
export AWS_S3_PUT_REGION=

### Who do I talk to? ###
Martin Blythe